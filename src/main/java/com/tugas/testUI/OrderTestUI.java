package com.tugas.testUI;

import java.sql.SQLException;
import java.util.List;

import com.tugas.domain.Order;
import com.tugas.domain.OrderItem;
import com.tugas.services.OrderService;

public class OrderTestUI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OrderTestUI.FindByOrderId(2);
	}
	public static void CreateOrder() {
		OrderService orderService = new OrderService();
		try {
			orderService.save();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void FindAll() {
		OrderService orderService = new OrderService();
		List<Order> orders;
		try {
			orders = orderService.findOrders(0);
			List <OrderItem> orderItems = orderService.findOrderItems(0);
			System.out.println(orders);
			System.out.println(orderItems);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void FindByOrderId(int orderId) {
		OrderService orderService = new OrderService();
		List<Order> orders;
		try {
			orders = orderService.findOrders(orderId);
			List <OrderItem> orderItems = orderService.findOrderItems(orderId);
			System.out.println(orders);
			System.out.println(orderItems);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
