package com.tugas.domain;

public class OrderItem {
	String code;
	String name;
	String type;
	double price;
	int qty;
	int orderId;

	public OrderItem(String code, String name, String type, double price, int qty, int orderId) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
		this.qty = qty;
		this.orderId = orderId;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getQty() {
		return qty;
	}
	
	public int getOrderId() {
		return orderId;
	}
	public String toString() {
		return "item purchased: #" + code + " " + name + " - type: " + type + " - price: " + price + " - amount: " + qty;
	}

}
