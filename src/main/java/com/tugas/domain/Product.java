package com.tugas.domain;

public class Product {
	String code;
	String name;
	String type;
	double price;
	int stock;
	int id;

	public Product(String code, String name, String type, double price, int stock, int id) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
		this.stock = stock;
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getStock() {
		return stock;
	}
	
	public int getId() {
		return id;
	}
	public String toString() {
		return "Product data of #" + code + " " + name + " - type: " + type + " - price: " + price + " - current stock: " + stock;
	}

}