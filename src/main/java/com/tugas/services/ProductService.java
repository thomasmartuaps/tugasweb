package com.tugas.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.tugas.domain.Product;
import com.tugas.repositories.ProductRepository;
import com.tugas.repositories.dbConnect;

public class ProductService {
	public ProductService() {
		
	}
	public int save(Product product) throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		ProductRepository productRepo = new ProductRepository(conn);
		int result = 0;
		try {
			conn.setAutoCommit(false);
			result = productRepo.save(product);
			dbConnect.commit(conn);
			dbConnect.close(conn);
		} catch (SQLException e) {
			dbConnect.rollback(conn);
			System.out.println("SQL error, rolling back...");
			e.printStackTrace();
			
		}
		System.out.println("Selesai create product");
		return result;
	}
	public int update(Product product) throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		ProductRepository productRepo = new ProductRepository(conn);
		int result = 0;
		try {
			conn.setAutoCommit(false);
			result = productRepo.update(product);
			dbConnect.commit(conn);
			dbConnect.close(conn);
		} catch (SQLException e) {
			dbConnect.rollback(conn);
			System.out.println("SQL error, rolling back...");
			e.printStackTrace();
			
		}
		System.out.println("Selesai update product");
		return result;
	}
	public List<Product> findProducts(int id) throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		
		ProductRepository productRepo = new ProductRepository(conn);
		List<Product> products = null;
		try {
			if (id > 0) {
				products = productRepo.findById(id);
			} else {
				products = productRepo.findAll();	
			}
			dbConnect.close(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	}
}
