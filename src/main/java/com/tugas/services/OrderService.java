package com.tugas.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.tugas.domain.Order;
import com.tugas.domain.OrderItem;
import com.tugas.repositories.OrderItemRepository;
import com.tugas.repositories.OrderRepository;
import com.tugas.repositories.dbConnect;

public class OrderService {
	public OrderService() {
		
	}
	public void save() throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		OrderItemRepository itemRepo = new OrderItemRepository(conn);
		OrderItem item = new OrderItem("025", "Pika", "mouse", 4000, 2, 7);
		OrderRepository orderRepo = new OrderRepository(conn);
		Order order = new Order(7, 0);
		try {
			conn.setAutoCommit(false);
			itemRepo.save(item);
			orderRepo.save(order);
			dbConnect.commit(conn);
			dbConnect.close(conn);
		} catch (SQLException e) {
			dbConnect.rollback(conn);
			System.out.println("SQL error, rolling back...");
			e.printStackTrace();
			
		}
		System.out.println("Selesai create order");
	}
	public List<Order> findOrders(int orderId) throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		OrderRepository orderRepo = new OrderRepository(conn);
		List<Order> orders = null;
		
		try {
			if (orderId > 0) {
				orders = orderRepo.findAll();				
			} else {
				orders = orderRepo.findById(orderId);
			}
			dbConnect.close(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;
	}
	public List<OrderItem> findOrderItems(int orderId) throws SQLException, ClassNotFoundException {
		Connection conn = dbConnect.getConnection("jdbc:mysql://localhost:3306/latihan_sql");
		OrderItemRepository itemRepo = new OrderItemRepository(conn);
		List<OrderItem> orderItems = null;
		
		try {
			if (orderId > 0) {
				orderItems = itemRepo.findAll();				
			} else {
				orderItems = itemRepo.findByOrderId(orderId);
			}
			dbConnect.close(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orderItems;
	}
}
