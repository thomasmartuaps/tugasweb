package com.tugas.repositories;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tugas.domain.Order;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class OrderRepository {
	private Connection conn;
	public OrderRepository(Connection conn) {
		// TODO Auto-generated method stub
		this.conn = conn;
	}
	public void print(Order order) {
		System.out.println(order.getId());
		System.out.println(new java.util.Date(order.getCreatedAt()));
	}
	public void save(Order order) throws SQLException {
		String sqlOrder = "insert into order_table(order_id, created_at) values(?,?)";
		
		PreparedStatement stm = conn.prepareStatement(sqlOrder);
		int Index = 0;
		stm.setInt(++Index, order.getId());
		stm.setDate(++Index, new java.sql.Date(order.getCreatedAt()));
		
		int result = stm.executeUpdate();

		return;
	}
	public List<Order> findAll() throws SQLException {
		String sql = "select * from order_table";
		
		PreparedStatement stm = conn.prepareStatement(sql);
		
		ResultSet res = stm.executeQuery();
		List<Order> list = new ArrayList<>();
		
		while (res.next()) {
			list.add(new Order(res.getInt("order_id"), res.getDate("created_at").getTime()));
		}
		
		return list;
	}
	public List<Order> findById(int orderId) throws SQLException {
		String sql = "select * from order_table WHERE order_id = ?";
				
		PreparedStatement stm = conn.prepareStatement(sql);
		stm.setInt(1, orderId);
		
		ResultSet res = stm.executeQuery();
		List<Order> list = new ArrayList<>();
		
		while (res.next()) {
			list.add(new Order(res.getInt("order_id"), res.getDate("created_at").getTime()));
		}
		
		return list;
	}
}
