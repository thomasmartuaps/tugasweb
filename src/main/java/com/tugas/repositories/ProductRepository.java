package com.tugas.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tugas.domain.Product;

public class ProductRepository {
	private Connection conn;
	public ProductRepository(Connection conn) {
		// TODO Auto-generated method stub
		this.conn = conn;
	}
	
	public void print(Product product) {
		System.out.println(product.getCode());
		System.out.println(product.getName());
	}
	public int save(Product product) throws SQLException {
		String sqlProduct = "insert into product_table(code, name, type, price, stock) values(?,?,?,?,?)";
		
		PreparedStatement stm = conn.prepareStatement(sqlProduct);
		int Index = 0;
		stm.setString(++Index, product.getCode());
		stm.setString(++Index, (product.getName()));
		stm.setString(++Index, (product.getType()));
		stm.setDouble(++Index, (product.getPrice()));
		stm.setInt(++Index, (product.getStock()));
		
		int result = stm.executeUpdate();

		return result;
	}
	public int update(Product product) throws SQLException {
		String sqlProduct = "update product_table set code = ?, name = ?, type = ?, price = ?, stock = ? WHERE id = ?";
		
		PreparedStatement stm = conn.prepareStatement(sqlProduct);
		int Index = 0;
		stm.setString(++Index, product.getCode());
		stm.setString(++Index, (product.getName()));
		stm.setString(++Index, (product.getType()));
		stm.setDouble(++Index, (product.getPrice()));
		stm.setInt(++Index, (product.getStock()));
		stm.setInt(++Index, (product.getId()));
		
		int result = stm.executeUpdate();
		
		return result;
	}
	public List<Product> findAll() throws SQLException {
		String sql = "select * from product_table";
		
		PreparedStatement stm = conn.prepareStatement(sql);
		
		ResultSet res = stm.executeQuery();
		List<Product> list = new ArrayList<>();
		while (res.next()) {
			list.add(new Product(res.getString("code"), res.getString("name"), res.getString("type"), res.getDouble("price"), res.getInt("stock"), res.getInt("id")));
		}
		return list;
	}
	public List<Product> findById(int id) throws SQLException {
		String sql = "select * from product_table WHERE id = ?";
				
		PreparedStatement stm = conn.prepareStatement(sql);
		stm.setInt(1, id);
		
		ResultSet res = stm.executeQuery();
		List<Product> list = new ArrayList<>();
		while (res.next()) {
			list.add(new Product(res.getString("code"), res.getString("name"), res.getString("type"), res.getDouble("price"), res.getInt("stock"), res.getInt("id")));
		}
		return list;
	}
}
